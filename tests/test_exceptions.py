import pytest


def myfunc():
    raise ValueError("Exception 123 raised")


def test_divide_by_zero():
    with pytest.raises(ZeroDivisionError) as e:
        num = 1 / 0

    assert "division by zero" in str(e.value)


def test_divide_by_zero_no_assert():
    with pytest.raises(ZeroDivisionError):
        num = 5 / 0


def test_match():
    with pytest.raises(ValueError, match=r".* 123 .*"):
        myfunc()
