import mathlib
import pytest

@pytest.mark.math
def test_one_plus_one():
    assert 1 + 1 == 2

@pytest.mark.math
def test_one_plus_two():
    a = 1
    b = 2
    c = 3
    assert a + b == c


@pytest.mark.math
def test_check_if_even():
    a = 10
    assert a % 2 == 0, "Value was odd, should be even"


@pytest.mark.math
def test_equivalence():
    a = 10
    b = 10.0
    assert a == b


def f():
    return 4


def test_function():
    assert f() == 4


@pytest.mark.math
def test_calc_addition():
    """Verify the output of calc addition function"""
    output = mathlib.calc_addition(2, 4)
    assert output == 6


# Parametrized tests example

# Multiplication ideas


products = [
    (7, 4, 28),  # Positive by positive (integer)
    (6, -7, -42),  # Positive by negative
    (-10, -10, 100),  # Negative  by negative
    (400, 1, 400),  # Positive by 1 (identity property of multiplication)
    (9999, 0, 0),  # Positive by zero
    (-9999, 0, 0),  # Negative by zero
    (0.75, 4, 3.0),  # Float by Float
]


@pytest.mark.math
@pytest.mark.parametrize('a,b,product', products)
def test_multiplication(a, b, product):
    assert a * b == product


# Testing all combinations of arguments:

@pytest.mark.math
@pytest.mark.parametrize("x", [0, 10])
@pytest.mark.parametrize("y", [2, 10])
def test_foo(x, y):
    assert x + y > 0


# xfailing within a test

@pytest.mark.math
@pytest.mark.parametrize(
    "test_input,expected",
    [("3+5", 8), ("2+4", 6), pytest.param("6*9", 42, marks=pytest.mark.xfail)],
)
def test_eval(test_input, expected):
    assert eval(test_input) == expected
